package com.blm.cloudstorage.service;

import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.repository.UserRepository;
import com.blm.cloudstorage.request.RegistrationRequest;
import com.blm.cloudstorage.service.exception.EntityAlreadyExistsException;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.exception.OutOfLimitException;
import com.blm.cloudstorage.service.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.math.BigInteger;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserService userService;

    @Test
    void register_Should_Throw_Exception() {
        when(userRepository.existsByEmail(any()))
                .thenReturn(true);

        RegistrationRequest request = new RegistrationRequest();
        request.setUsername("username");
        request.setEmail("Email@my.com");
        request.setPassword("password");

        assertThrows(EntityAlreadyExistsException.class, () -> {
            userService.register(request);
        });
    }

    @Test
    void register_Should_Register_New_User() throws EntityAlreadyExistsException {
        when(userRepository.existsByEmail(any()))
                .thenReturn(false);

        RegistrationRequest request = new RegistrationRequest();
        request.setUsername("username");
        request.setEmail("Email@my.com");
        request.setPassword("password");

        userService.register(request);
    }

    @Test
    void withdrawUsedStorageSpaceForUser_Should_Withdrow_Correctly() throws OutOfLimitException, EntityNotFoundException {
        Long userId = 3L;
        User user = new User();
        user.setId(userId);
        user.setUsedStorageSpaceInBytes(new BigInteger("512"));
        user.setStorageLimitInBytes(new BigInteger("1024"));

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.of(user));

        userService.withdrawUsedStorageSpaceForUser(userId, new BigInteger("500"));

        assertEquals(user.getUsedStorageSpaceInBytes(), new BigInteger("12"));
    }

    @Test
    void withdrawUsedStorageSpaceForUser_Should_Throw_OutOfLimitException() {
        Long userId = 3L;
        User user = new User();
        user.setId(userId);
        user.setUsedStorageSpaceInBytes(new BigInteger("512"));
        user.setStorageLimitInBytes(new BigInteger("512"));

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.of(user));

        System.out.println(user);

        assertThrows(OutOfLimitException.class, () -> {
            userService.withdrawUsedStorageSpaceForUser(userId, new BigInteger("1024"));
        });
    }

    @Test
    void withdrawUsedStorageSpaceForUser_Should_Throw_EntityNotFoundException() {
        Long userId = 3L;

        User user = new User();
        user.setId(userId);

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            userService.withdrawUsedStorageSpaceForUser(userId, new BigInteger("1024"));
        });
    }

    @Test
    void addUsedStorageSpaceForUser_Should_Add_Space() throws OutOfLimitException, EntityNotFoundException {
        Long userId = 3L;
        User user = new User();
        user.setId(userId);
        user.setUsedStorageSpaceInBytes(new BigInteger("512"));
        user.setStorageLimitInBytes(new BigInteger("1024"));

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.of(user));

        userService.addUsedStorageSpaceForUser(userId, new BigInteger("512"));
    }

    @Test
    void addUsedStorageSpaceForUser_Should_Throw_EntityNotFoundException() {
        Long userId = 3L;
        User user = new User();
        user.setId(userId);
        user.setUsedStorageSpaceInBytes(new BigInteger("1024"));
        user.setStorageLimitInBytes(new BigInteger("1024"));

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            userService.addUsedStorageSpaceForUser(userId, new BigInteger("512").negate());
        });
    }
}