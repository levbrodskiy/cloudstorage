package com.blm.cloudstorage.service;

import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.repository.BucketRepository;
import com.blm.cloudstorage.repository.UserRepository;
import com.blm.cloudstorage.request.BucketRequest;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.mapper.BucketMapper;
import com.blm.cloudstorage.service.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BucketServiceTest {
    @Mock
    private UserService userService;

    @Mock
    private BucketMapper bucketMapper;

    @Mock
    private BucketRepository bucketRepository;

    @InjectMocks
    private BucketService bucketService;

    @Test
    void addBucket() throws EntityNotFoundException {
        BucketRequest bucketRequest = new BucketRequest();
        bucketRequest.setTitle("title");
        bucketRequest.setDescription("desc");

        when(userService.getCurrentUser(any()))
                .thenReturn(java.util.Optional.of(new User()));

        bucketService.addBucket(new UsernamePasswordAuthenticationToken("", ""), bucketRequest);
    }

    @Test
    void addBucket_Should_Throw_EntityNotFoundException() {
        BucketRequest bucketRequest = new BucketRequest();
        bucketRequest.setTitle("title");
        bucketRequest.setDescription("desc");

        when(userService.getCurrentUser(any()))
                .thenReturn(java.util.Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            bucketService.addBucket(new UsernamePasswordAuthenticationToken("", ""), bucketRequest);
        });
    }

    @Test
    void deleteBucket() {
        Long bucketId = 5L;

        when(userService.getCurrentUser(any()))
                .thenReturn(java.util.Optional.of(new User()));

        when(bucketRepository.deleteBucketByIdAndOwner(eq(bucketId), any()))
                .thenReturn(false);

        assertThrows(EntityNotFoundException.class, () -> {
            bucketService.deleteBucket(new UsernamePasswordAuthenticationToken("", ""), bucketId);
        });
    }

    @Test
    void deleteBucket_Should_Throw_EntityNotFoundException() throws EntityNotFoundException {
        Long bucketId = 5L;

        when(userService.getCurrentUser(any()))
                .thenReturn(java.util.Optional.of(new User()));

        when(bucketRepository.deleteBucketByIdAndOwner(eq(bucketId), any()))
                .thenReturn(true);

        bucketService.deleteBucket(new UsernamePasswordAuthenticationToken("", ""), bucketId);
    }
}