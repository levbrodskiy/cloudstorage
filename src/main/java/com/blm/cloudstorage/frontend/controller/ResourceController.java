package com.blm.cloudstorage.frontend.controller;

import com.blm.cloudstorage.domain.Resource;
import com.blm.cloudstorage.response.BucketResponse;
import com.blm.cloudstorage.response.ResourceMetaResponse;
import com.blm.cloudstorage.service.BucketService;
import com.blm.cloudstorage.service.ResourceService;
import com.blm.cloudstorage.service.exception.EmptyResourceException;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.exception.OutOfLimitException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/buckets/{bucket_id}/resources")
public class ResourceController {
    private final ResourceService resourceService;
    private final BucketService bucketService;

    @Autowired
    public ResourceController(ResourceService resourceService, BucketService bucketService) {
        this.resourceService = resourceService;
        this.bucketService = bucketService;
    }

    @GetMapping
    public String getResources(Model model,
                               @AuthenticationPrincipal Principal principal,
                               @PathVariable(name = "bucket_id") Long bucketId,
                               @RequestParam(name = "search", required = false) String search) {
        try {
            List<ResourceMetaResponse> response = resourceService.getResourceMeta(principal, bucketId, search);
            BucketResponse bucketInfo = bucketService.getBucket(principal,bucketId);
            model.addAttribute("bucket",bucketInfo);
            model.addAttribute("resources",response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "resources";
    }

    @ResponseBody
    @GetMapping("/{id}")
    public ResponseEntity getResource(@AuthenticationPrincipal Principal principal,
                              @PathVariable(name = "id") Long id,
                                      HttpServletResponse response) {
        try {
            Resource resource = resourceService.getResource(principal, id);
            System.out.println(resource.getPath());
            File file = new File(resource.getPath());

            byte[] data = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            ByteArrayResource resource2 = new ByteArrayResource(data);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM) //
                    .contentLength(data.length) //
                    .body(resource2);
        } catch (EntityNotFoundException | IOException e) {

            e.printStackTrace();
        }
        return null;
    }

    @PostMapping
    public String addResource(@AuthenticationPrincipal Principal principal,
                              @PathVariable(name = "bucket_id") Long bucketId,
                              @RequestParam(name = "name") String name,
                              MultipartFile file) {
        try {
            resourceService.addResource(principal, bucketId, file, name);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfLimitException e) {
            e.printStackTrace();
        } catch (EmptyResourceException e) {
            e.printStackTrace();
        }

        return "redirect:/buckets/"+ bucketId + "/resources";
    }

    @PostMapping(value = "/{id}/delete")
    public String deleteResource(@AuthenticationPrincipal Principal principal,
                                 @PathVariable(name = "id") Long id) {
        try {
            resourceService.deleteResource(principal, id);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfLimitException e) {
            e.printStackTrace();
        }

        return "redirect:/resources";
    }
}
