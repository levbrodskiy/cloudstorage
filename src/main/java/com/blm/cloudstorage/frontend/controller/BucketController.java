package com.blm.cloudstorage.frontend.controller;

import com.blm.cloudstorage.request.BucketRequest;
import com.blm.cloudstorage.response.BucketResponse;
import com.blm.cloudstorage.service.BucketService;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/buckets")
public class BucketController {
    private final BucketService bucketService;

    @Autowired
    public BucketController(BucketService bucketService) {
        this.bucketService = bucketService;
    }

    @GetMapping
    public String getBuckets(Model model,
                             @AuthenticationPrincipal Principal principal) {
        try {
            List<BucketResponse> response = bucketService.getBucketsForUser(principal);
            model.addAttribute("buckets", response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return "buckets";
    }

    @GetMapping("/add")
    public String addBucketPage() {
        return "add_bucket";
    }

    @PostMapping
    public String addBucket(@AuthenticationPrincipal Principal principal,
                            @Valid BucketRequest request) {
        try {
            bucketService.addBucket(principal, request);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "redirect:/buckets";
    }

    @GetMapping(value = "/{id}/edit")
    public String updateBucketPage(Model model,
                                   @AuthenticationPrincipal Principal principal,
                               @PathVariable(name = "id") Long id) {
        try {
            BucketResponse response = bucketService.getBucket(principal, id);
            model.addAttribute("bucket",response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "redirect:/buckets";
    }

    @PutMapping(value = "/{id}")
    public String updateBucket(@AuthenticationPrincipal Principal principal,
                               @PathVariable(name = "id") Long id,
                               @Valid BucketRequest request) {
        try {
            bucketService.updateBucket(principal, id, request);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "edit_bucket";
    }

    @PostMapping(value = "/{id}/delete")
    public String deleteBucket(@AuthenticationPrincipal Principal principal,
                               @PathVariable(name = "id") Long id) {
        try {
            bucketService.deleteBucket(principal, id);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "redirect:/buckets";
    }
}
