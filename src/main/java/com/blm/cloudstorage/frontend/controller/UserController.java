package com.blm.cloudstorage.frontend.controller;

import com.blm.cloudstorage.request.RegistrationRequest;
import com.blm.cloudstorage.request.UserProfileRequest;
import com.blm.cloudstorage.response.UserProfileResponse;
import com.blm.cloudstorage.service.UserService;
import com.blm.cloudstorage.service.exception.EntityAlreadyExistsException;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.security.Principal;

@Controller
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/registration")
    public String getRegistrationPage() {
        return "registration";
    }

    @PostMapping(value = "/registration")
    public String register(RegistrationRequest request) {
        System.out.println(request);
        try {
            userService.register(request);
        } catch (EntityAlreadyExistsException e) {
            e.printStackTrace();
        }

        return "redirect:/profile";
    }

    @GetMapping(value = "/profile")
    public String getProfile(@AuthenticationPrincipal Principal principal,
                             Model model) {
        try {
            UserProfileResponse response = userService.getUserProfile(principal);
            model.addAttribute("user", response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }


        return "profile";
    }

    @PutMapping(value = "/profile")
    public String updateProfile(@AuthenticationPrincipal Principal principal,
                                UserProfileRequest request) {
        try {
            userService.updateUserProfile(principal, request);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "profile";
    }
}
