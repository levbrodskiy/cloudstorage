package com.blm.cloudstorage.repository;

import com.blm.cloudstorage.domain.Bucket;
import com.blm.cloudstorage.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BucketRepository extends JpaRepository<Bucket, Long> {
    Optional<Bucket> findBucketByIdAndOwner(Long id, User owner);
    boolean deleteBucketByIdAndOwner(Long id, User owner);
    List<Bucket> findAllByOwner(User owner);
}
