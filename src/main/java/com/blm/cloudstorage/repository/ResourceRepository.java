package com.blm.cloudstorage.repository;

import com.blm.cloudstorage.domain.Bucket;
import com.blm.cloudstorage.domain.Resource;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long> {
    @EntityGraph(attributePaths = "bucket")
    Optional<Resource> findById(Long id);

    List<Resource> findAllByBucketAndNameLike(Bucket bucket, String name);
    List<Resource> findAllByBucket(Bucket bucket);
}
