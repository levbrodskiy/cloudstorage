package com.blm.cloudstorage.service;

import com.blm.cloudstorage.domain.Bucket;
import com.blm.cloudstorage.domain.Resource;
import com.blm.cloudstorage.domain.ResourceTypeName;
import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.repository.ResourceRepository;
import com.blm.cloudstorage.response.ResourceMetaResponse;
import com.blm.cloudstorage.service.exception.EmptyResourceException;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.exception.OutOfLimitException;
import com.blm.cloudstorage.service.mapper.ResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Lazy
@Service
public class ResourceService {
    @Value("${resources.storage.path}")
    private String storagePath;
    private final UserService userService;
    private final BucketService bucketService;
    private final ResourceRepository resourceRepository;
    private final ResourceMapper resourceMapper;

    @Autowired
    public ResourceService(UserService userService, BucketService bucketService, ResourceRepository resourceRepository, ResourceMapper resourceMapper) {
        this.userService = userService;
        this.bucketService = bucketService;
        this.resourceRepository = resourceRepository;
        this.resourceMapper = resourceMapper;
    }

    @Transactional(rollbackFor = {IOException.class, EntityNotFoundException.class, OutOfLimitException.class})
    public void addResource(Principal principal, Long bucketId, MultipartFile file, String name) throws EntityNotFoundException, IOException, OutOfLimitException, EmptyResourceException {
        if (file.isEmpty()) {
            throw new EmptyResourceException();
        }

        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = bucketService.findByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException(""));

        ResourceTypeName type = getType(name);

        Resource resource = new Resource();
        resource.setId(null);
        resource.setName(name);
        resource.setBucket(bucket);
        resource.setType(type);
        resource.setSizeInBytes(file.getSize());

        userService.addUsedStorageSpaceForUser(user.getId(), BigInteger.valueOf(resource.getSizeInBytes()));

        resourceRepository.save(resource);

        Path path = Paths.get(storagePath, resource.getId()  + File.separatorChar + file.getOriginalFilename());

        Files.write(path, file.getBytes(), StandardOpenOption.CREATE_NEW);

        resource.setPath(path.toString());

        resourceRepository.save(resource);
    }

    private ResourceTypeName getType(String name) {
        ResourceTypeName type = ResourceTypeName.UNDEFINED;

        if (name.endsWith("jpg") || name.endsWith("png")) {
            type = ResourceTypeName.IMAGE;
        }

        if (name.endsWith("mp3")) {
            type = ResourceTypeName.IMAGE;
        }

        if (name.endsWith("mp4")) {
            type = ResourceTypeName.MEDIA;
        }

        if (name.endsWith("txt") || name.endsWith("doc") || name.endsWith("docx")) {
            type = ResourceTypeName.TEXT;
        }

        return type;
    }

    @Transactional(rollbackFor = IOException.class)
    public void deleteResource(Principal principal, Long resourceId) throws EntityNotFoundException, IOException, OutOfLimitException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        deleteResource(user, resourceId);
    }

    @Transactional
    void deleteResourceWithoutBucketCheck(User user, Resource resource) throws EntityNotFoundException, OutOfLimitException, IOException {

        userService.withdrawUsedStorageSpaceForUser(user.getId(), BigInteger.valueOf(resource.getSizeInBytes()));

        Files.delete(Paths.get(resource.getPath()));
    }

    @Transactional
    void deleteResource(User user, Long id) throws EntityNotFoundException, OutOfLimitException, IOException {
        Resource resource = resourceRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = resource.getBucket();

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        userService.withdrawUsedStorageSpaceForUser(user.getId(), BigInteger.valueOf(resource.getSizeInBytes()));

        Files.delete(Paths.get(resource.getPath()));
    }

    @Transactional(rollbackFor = IOException.class)
    public void deleteResources(Principal principal, Long bucketId) throws EntityNotFoundException, IOException, OutOfLimitException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = bucketService.findByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException(""));

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        for (Resource resource : resourceRepository.findAllByBucket(bucket)) {
            deleteResourceWithoutBucketCheck(user, resource);
        }
    }

    @Transactional
    public Resource getResource(Principal principal, Long resourceId) throws EntityNotFoundException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Resource resource = resourceRepository.findById(resourceId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = resource.getBucket();

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        return resource;
    }

    @Transactional
    public List<ResourceMetaResponse> getResourceMeta(Principal principal, Long bucketId) throws EntityNotFoundException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = bucketService.findByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException(""));

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        List<Resource> resources = resourceRepository.findAllByBucket(bucket);

        return resources
                .stream()
                .map(resourceMapper::toResourceMetaResponse)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<ResourceMetaResponse> getResourceMeta(Principal principal, Long bucketId, String search) throws EntityNotFoundException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = bucketService.findByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException(""));

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        List<Resource> resources = new ArrayList<>();

        if (search == null) {
            resources = resourceRepository.findAllByBucket(bucket);
        } else {
            search = "%" + search + "%";
            resources = resourceRepository.findAllByBucketAndNameLike(bucket, search);
        }

        return resources
                .stream()
                .map(resourceMapper::toResourceMetaResponse)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<ResourceMetaResponse> getResourceMetaLikeName(Principal principal, Long bucketId, String name) throws EntityNotFoundException {
        User user = userService.getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Bucket bucket = bucketService.findByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException(""));

        if (!bucket.getOwner().getId().equals(user.getId())) {
            throw new EntityNotFoundException("");
        }

        String queryName = "%" + name + "%";

        List<Resource> resources = resourceRepository.findAllByBucketAndNameLike(bucket, queryName);

        return resources
                .stream()
                .map(resourceMapper::toResourceMetaResponse)
                .collect(Collectors.toList());
    }
}


