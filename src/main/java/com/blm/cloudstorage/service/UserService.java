package com.blm.cloudstorage.service;

import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.repository.UserRepository;
import com.blm.cloudstorage.request.RegistrationRequest;
import com.blm.cloudstorage.request.UserProfileRequest;
import com.blm.cloudstorage.response.UserProfileResponse;
import com.blm.cloudstorage.security.DefaultUserDetails;
import com.blm.cloudstorage.service.exception.EntityAlreadyExistsException;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.exception.OutOfLimitException;
import com.blm.cloudstorage.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.Principal;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Transactional
    public void register(RegistrationRequest request) throws EntityAlreadyExistsException {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new EntityAlreadyExistsException("User with this email already exists");
        }

        User user = new User();
        user.setUsedStorageSpaceInBytes(new BigInteger("0"));

        int limit = 1024 * 1024 * 8;

        user.setStorageLimitInBytes(new BigInteger(String.valueOf(limit)));

        userMapper.mergeIntoUser(user, request);

        userRepository.save(user);
    }

    @Transactional
    public UserProfileResponse getUserProfile(Principal principal) throws EntityNotFoundException {
        User user = getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        return userMapper.toProfileResponse(user);
    }

    @Transactional
    public UserProfileResponse updateUserProfile(Principal principal, UserProfileRequest request) throws EntityNotFoundException {
        User user = getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        userMapper.mergeIntoUser(user, request);

        user = userRepository.save(user);

        return userMapper.toProfileResponse(user);
    }

    @Transactional
    public Optional<User> getCurrentUser(Principal principal) {
        if ( !(principal instanceof  Authentication) ) {
            return Optional.empty();
        }

        Authentication authentication = (Authentication)principal;

        if ( !(authentication.getPrincipal() instanceof DefaultUserDetails) ) {
            return Optional.empty();
        }

        DefaultUserDetails userDetails = (DefaultUserDetails) authentication.getPrincipal();

        return Optional.ofNullable(userDetails.getUser());
    }

    @Transactional
    public void withdrawUsedStorageSpaceForUser(Long userId, BigInteger bytesCount) throws OutOfLimitException, EntityNotFoundException {
        User user = userRepository
                .findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        BigInteger usedStorageSpace = user.getUsedStorageSpaceInBytes();

        usedStorageSpace = usedStorageSpace.subtract(bytesCount);

        if (usedStorageSpace.compareTo(BigInteger.ZERO) < 0) {
            throw new OutOfLimitException();
        }

        user.setUsedStorageSpaceInBytes(usedStorageSpace);

        userRepository.save(user);
    }

    @Transactional
    public void addUsedStorageSpaceForUser(Long userId, BigInteger bytesCount) throws OutOfLimitException, EntityNotFoundException {
        User user = userRepository
                .findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        BigInteger usedStorageSpace = user.getUsedStorageSpaceInBytes();

        usedStorageSpace = usedStorageSpace.add(bytesCount);

        if (usedStorageSpace.compareTo(user.getStorageLimitInBytes()) > 0) {
            throw new OutOfLimitException();
        }

        user.setUsedStorageSpaceInBytes(usedStorageSpace);

        userRepository.save(user);
    }
}
