package com.blm.cloudstorage.service.mapper;

import com.blm.cloudstorage.domain.Bucket;
import com.blm.cloudstorage.request.BucketRequest;
import com.blm.cloudstorage.response.BucketResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface BucketMapper {

    void mergeIntoBucket(@MappingTarget Bucket bucket, BucketRequest request);

    BucketResponse toResponse(Bucket bucket);
}
