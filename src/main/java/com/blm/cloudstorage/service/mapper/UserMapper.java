package com.blm.cloudstorage.service.mapper;

import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.request.RegistrationRequest;
import com.blm.cloudstorage.request.UserProfileRequest;
import com.blm.cloudstorage.response.UserProfileResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {

    void mergeIntoUser(@MappingTarget User user, RegistrationRequest request);
    UserProfileResponse toProfileResponse(User user);
    void mergeIntoUser(@MappingTarget User user, UserProfileRequest request);
}
