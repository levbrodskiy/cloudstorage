package com.blm.cloudstorage.service.mapper;

import com.blm.cloudstorage.domain.Resource;
import com.blm.cloudstorage.response.ResourceMetaResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ResourceMapper {
    ResourceMetaResponse toResourceMetaResponse(Resource resource);
}
