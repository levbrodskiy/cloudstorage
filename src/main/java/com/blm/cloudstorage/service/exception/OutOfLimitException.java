package com.blm.cloudstorage.service.exception;

public class OutOfLimitException extends Exception {
    public OutOfLimitException() {
    }

    public OutOfLimitException(String message) {
        super(message);
    }

    public OutOfLimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutOfLimitException(Throwable cause) {
        super(cause);
    }

    public OutOfLimitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
