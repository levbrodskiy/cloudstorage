package com.blm.cloudstorage.service;

import com.blm.cloudstorage.domain.Bucket;
import com.blm.cloudstorage.domain.User;
import com.blm.cloudstorage.repository.BucketRepository;
import com.blm.cloudstorage.request.BucketRequest;
import com.blm.cloudstorage.response.BucketResponse;
import com.blm.cloudstorage.service.exception.EntityNotFoundException;
import com.blm.cloudstorage.service.mapper.BucketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Lazy
@Service
public class BucketService {
    private final BucketRepository bucketRepository;
    private final BucketMapper bucketMapper;
    private final UserService userService;

    @Autowired
    public BucketService(BucketRepository bucketRepository, BucketMapper bucketMapper, UserService userService) {
        this.bucketRepository = bucketRepository;
        this.bucketMapper = bucketMapper;
        this.userService = userService;
    }

    @Transactional
    public BucketResponse addBucket(Principal principal, BucketRequest request) throws EntityNotFoundException {
        User user = userService
                .getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        Bucket bucket = new Bucket();
        bucket.setId(null);
        bucket.setOwner(user);

        bucketMapper.mergeIntoBucket(bucket, request);

        bucket = bucketRepository.save(bucket);

        return bucketMapper.toResponse(bucket);
    }

    @Transactional
    public void updateBucket(Principal principal, Long bucketId, BucketRequest request) throws EntityNotFoundException {
        User user = userService
                .getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        Bucket bucket = bucketRepository
                .findBucketByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException("Bucket with this id not found"));

        bucketMapper.mergeIntoBucket(bucket, request);

        bucketRepository.save(bucket);
    }

    @Transactional
    public void deleteBucket(Principal principal, Long bucketId) throws EntityNotFoundException {
        User user = userService
                .getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        boolean deleted = bucketRepository.deleteBucketByIdAndOwner(bucketId, user);

        if (!deleted) {
            throw new EntityNotFoundException("Bucket with this id not found");
        }

    }

    @Transactional(readOnly = true)
    public List<BucketResponse> getBucketsForUser(Principal principal) throws EntityNotFoundException {
        User user = userService
                .getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        List<Bucket> buckets = bucketRepository.findAllByOwner(user);

        return buckets
                .stream()
                .map(bucketMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public BucketResponse getBucket(Principal principal, Long bucketId) throws EntityNotFoundException {
        User user = userService
                .getCurrentUser(principal)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        Bucket bucket = bucketRepository.findBucketByIdAndOwner(bucketId, user)
                .orElseThrow(() -> new EntityNotFoundException("Bucket with this id not found"));

        return bucketMapper.toResponse(bucket);
    }

    @Transactional(readOnly = true)
    public Optional<Bucket> findByIdAndOwner(Long id, User owner) {
        return bucketRepository.findBucketByIdAndOwner(id, owner);
    }
}
