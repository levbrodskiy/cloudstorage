package com.blm.cloudstorage.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class BucketRequest {
    @NotBlank
    @Size(min = 3, max = 256)
    private String title;
    private String description;
}
