package com.blm.cloudstorage.request;

import lombok.Data;

@Data
public class UserProfileRequest {
    private String username;
}
