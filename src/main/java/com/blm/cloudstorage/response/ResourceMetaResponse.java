package com.blm.cloudstorage.response;

import com.blm.cloudstorage.domain.ResourceTypeName;
import lombok.Data;

@Data
public class ResourceMetaResponse {
    private Long id;
    private String name;
    private ResourceTypeName type;
}
