package com.blm.cloudstorage.response;

import lombok.Data;

@Data
public class BucketResponse {
    private Long id;
    private String title;
    private String description;
}
