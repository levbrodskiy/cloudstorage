package com.blm.cloudstorage.response;

import lombok.Data;

@Data
public class ResourceResponse {
    private String name;
    private byte[] data;
}
