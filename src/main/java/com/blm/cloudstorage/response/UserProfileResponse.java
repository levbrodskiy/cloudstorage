package com.blm.cloudstorage.response;

import lombok.Data;

@Data
public class UserProfileResponse {
    private String email;
    private String nickname;
}
