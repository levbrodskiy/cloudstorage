package com.blm.cloudstorage.domain;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN
}
