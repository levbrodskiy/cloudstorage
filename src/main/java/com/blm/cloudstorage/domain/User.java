package com.blm.cloudstorage.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String username;
    private String password;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "updated_at")
    private Date updatedAt;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;
    @Column(name = "used_storage_space_in_bytes")
    private BigInteger usedStorageSpaceInBytes;
    @Column(name = "storage_limit_in_bytes")
    private BigInteger storageLimitInBytes;
    @PrePersist
    private void prePersist() {
        createdAt = new Date();
        updatedAt = createdAt;
    }

    @PreUpdate
    private void preUpdate() {
        updatedAt = new Date();
    }
}
