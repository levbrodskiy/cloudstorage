package com.blm.cloudstorage.domain;

public enum ResourceTypeName {
    IMAGE, TEXT, AUDIO, MEDIA, UNDEFINED
}
